package ng.uplift.chatbot.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.github.library.bubbleview.BubbleTextView;

import java.util.List;

import ng.uplift.chatbot.Model.CustomChatModel;
import ng.uplift.chatbot.R;

/**
 * Created by ABDULMUJIB on 8/5/2017.
 */

public class CustomAdapter extends BaseAdapter {

    private List<CustomChatModel> customChatModels;
    private Context context;
    private LayoutInflater layoutInflater;

    public CustomAdapter(List<CustomChatModel> customChatModels, Context context){
        this.customChatModels = customChatModels;
        this.context = context;
        layoutInflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {

        return customChatModels.size();
    }

    @Override
    public Object getItem(int position) {
        return customChatModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        //here we check message is send or receive to inflate our layout
        if(view == null){
            if(customChatModels.get(position).isSend)
                view = layoutInflater.inflate(R.layout.list_message_send, null);
            else
                view = layoutInflater.inflate(R.layout.list_message_recieve, null);


            BubbleTextView bubbleTextView = (BubbleTextView)view.findViewById(R.id.text_msg);
            bubbleTextView.setText(customChatModels.get(position).message);
        }
        return view;
    }
}
