package ng.uplift.chatbot.Helper;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by ABDULMUJIB on 8/5/2017.
 */
//we will use this class to get result from API point
public class HttpDataHandler {
    public String stream = null;

    public HttpDataHandler(){

    }
    public String getHttpData(String urlstring){
        try{

            URL url = new URL(urlstring);
            HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();

            if(httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK){
                InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null)
                    stringBuilder.append(line);

                stream = stringBuilder.toString();
                httpURLConnection.disconnect();
            }

        }catch (Exception ex){
            stream = "Check your Connection...";
        }

        return stream;
    }

}
