package ng.uplift.chatbot;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import ng.uplift.chatbot.Adapter.CustomAdapter;
import ng.uplift.chatbot.Helper.HttpDataHandler;
import ng.uplift.chatbot.Model.ChatBotModel;
import ng.uplift.chatbot.Model.CustomChatModel;

public class MainActivity extends AppCompatActivity {
    ListView listView;
    EditText editText;
    List<CustomChatModel> customChatModels = new ArrayList<>();
    FloatingActionButton btnAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView)findViewById(R.id.list_of_msgs);
        editText = (EditText)findViewById(R.id.user_msg);

        btnAction = (FloatingActionButton)findViewById(R.id.fab);

        //get text from user and create chat model
        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String txt = editText.getText().toString();
                CustomChatModel ccm = new CustomChatModel(txt,true);//pass the text from the user and set isSend it to true(user send msg
                customChatModels.add(ccm);
                new ChatBotAPI().execute(customChatModels);//this class handle data from the api

                //remove user's text from the text Field after click the button
                editText.setText("");
            }
        });
    }

    class ChatBotAPI extends AsyncTask<List<CustomChatModel>,Void,String>{
        String stream = null;
        List<CustomChatModel> ccModel;
        String txt = editText.getText().toString();



        //pass parameters API key and text from the user to API point
        protected String doInBackground(List<CustomChatModel>... params) {

            try {
                String txtEncode = URLEncoder.encode(txt,"UTF-8").toString();

                String url = String.format("http://sandbox.api.simsimi.com/request.p?key=%s&lc=en&ft=1.0&text=%s",getString(R.string.simi_api),txtEncode);
                ccModel = params[0];//set list CustomModel to local variable
                //get result from API and use class HttpDataHandler to request response
                if(ccModel == null){
                    return null;
                }
                 HttpDataHandler httpDataHandler = new HttpDataHandler();
                stream = httpDataHandler.getHttpData(url);
            } catch (UnsupportedEncodingException e) {
               stream = "No Internet Connection";
            }
            return stream;

        }

        //this override method set date to listView
        protected void onPostExecute(String string){
            if(string == null){
                return;
            }
            Gson gson = new Gson();//use Gson object and create a custom chat model
            ChatBotModel response = gson.fromJson(string, ChatBotModel.class);

            CustomChatModel chatModel = new CustomChatModel(response.getResponse(),false);//response from sandbox API
            ccModel.add(chatModel);
            CustomAdapter ca = new CustomAdapter(ccModel, getApplicationContext());
            listView.setAdapter(ca);


        }
    }
}
