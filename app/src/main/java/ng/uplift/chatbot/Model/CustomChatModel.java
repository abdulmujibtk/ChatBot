package ng.uplift.chatbot.Model;

/**
 * Created by ABDULMUJIB on 8/5/2017.
 */

public class CustomChatModel {
    public String message;
    public boolean isSend;

    public CustomChatModel(String message, boolean isSend){
        this.message = message;
        this.isSend = isSend;
    }

    public boolean getIsSend(){
        return isSend;
    }

    public void setSend(boolean send) {
        this.isSend = send;
    }

    public String getMessage(){
        return message;
    }
    public void setMessage(String message){
        this.message = message;
    }

    public CustomChatModel(){

    }
}
