package ng.uplift.chatbot.Model;

/**
 * Created by ABDULMUJIB on 8/5/2017.
 */

public class ChatBotModel {

    public String response;
    public String id;
    public int result;
    public String message;

    public ChatBotModel(String response, String id, int result, String message){
        this.response = response;
        this.id = id;
        this.result = result;
        this.message = message;
    }

    public String getResponse(){
        if(response == null){
            return "Error......";
        }else {
            return response;
        }
    }

    public void setResponse(String response) {
        this.response = response;
    }
    public int getResult(){
        return  result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public String getId(){
        return id;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getMessage(){
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public ChatBotModel(){

    }
}
